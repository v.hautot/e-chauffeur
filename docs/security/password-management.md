# Politique de mots de passe & comptes utilisateur

## Création d'un compte utilisateur

Pour créer un compte utilisateur, il est nécessaire d'avoir un mail dont l'entrée MX réponds sur une requête DNS 
d'une part, et dont le domaine soit présent sur une liste d'autorisation d'autre part.

## Algorithme

L'API utilise Bcrypt, pour hasher les mots de passes de manière systématique, avec un grain de sel (salt).  
Ce hash se fait sur 10 itérations.

## Expiration du mot de passe

Tout mot de passe créé après la publication de la version 1.4 expirera passé 3 mois d'utilisation.

## Complexité du mot de passe

Le mot de passe doit faire au moins 8 caractères, tout caractère UTF-8 est valide, il n'y a pas de longueur maximale. 

## Connexion alternative

Il est possible de générer un jeton de connexion unique, envoyé sur l'email de l'inscrit, ce jeton : 
- Possède trois essais de connexion maximum
- Est valide une heure
- Ne peut être prolongé

L'objectif de ce jeton est un renouvellement de mot de passe en cas de perte ou d'expiration.
