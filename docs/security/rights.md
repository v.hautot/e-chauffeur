# Gestion des droits

Les rôles sont des groupes de droits donnant accès à des fonctionnalités au niveau de l'API du projet.

## Moindre privilège

L'utilisateur anonyme a un accès restreint sur quelques points d'API (contact, login, inscription).
Chaque rôle supplémentaire ajoute des droits, par défaut l'utilisateur n'a pas d'attribué des droits auxquels il n'a pas utilité.

## Rôles

| Rôle | Description | Restreint à une base |
|------|-------------|----------------------|
| ROLE_ANONYMOUS | Utilisateur non connecté | Non |
| ROLE_USER | Utilisateur connecté | Non |
| ROLE_DRIVER | Chauffeur | Oui |
| ROLE_REGULATEUR | Regulation | Oui |
| ROLE_ADMIN | Responsable d'une base | Oui |
| ROLE_SUPERADMIN | Super administrateur | Non |

## Droits

Merci de vous référer à la [documentation des droits](../rights.md).

## Sécurisation des fonctions d'administration

Les super administrateurs ont un accès comme tout utilisateur : nominatif, 
