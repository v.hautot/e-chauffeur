# Notes de sécurité

- [Injections SQL](./security/sql-injection.md)
- [Stockage des mots de passe](./security/password-management.md)
- [Chiffrement](./security/ssl.md)
- [Gestion des droits](./security/rights.md)
